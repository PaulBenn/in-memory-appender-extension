package uk.co.paulbenn.inmemoryappenderextension;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import org.slf4j.event.Level;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class InMemoryAppender<E extends ILoggingEvent> extends ListAppender<E> {
    public List<E> getEvents() {
        return list;
    }

    public int getNumberOfEvents() {
        return list.size();
    }

    public boolean contains(Level level, String message) {
        Pattern pattern = Pattern.compile(".*?" + level.toString() + ".*?" + Pattern.quote(message) + ".*?");

        return this.list.stream().anyMatch(event -> pattern.matcher(event.toString()).matches());
    }

    public void clear() {
        list = new ArrayList<>();
    }
}
