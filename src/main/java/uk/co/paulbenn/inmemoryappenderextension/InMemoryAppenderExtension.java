package uk.co.paulbenn.inmemoryappenderextension;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.slf4j.LoggerFactory;

public class InMemoryAppenderExtension implements BeforeAllCallback, BeforeEachCallback, ParameterResolver {
    private final InMemoryAppender<ILoggingEvent> inMemAppender = new InMemoryAppender<>();

    @Override
    public void beforeAll(ExtensionContext context) {
        Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        inMemAppender.start();
        rootLogger.addAppender(inMemAppender);
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        inMemAppender.clear();
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return InMemoryAppender.class.isAssignableFrom(parameterContext.getParameter().getType());
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return inMemAppender;
    }
}
