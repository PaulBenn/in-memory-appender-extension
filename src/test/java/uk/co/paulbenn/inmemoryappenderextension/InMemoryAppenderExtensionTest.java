package uk.co.paulbenn.inmemoryappenderextension;

import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(InMemoryAppenderExtension.class)
public class InMemoryAppenderExtensionTest {

    private static final Logger logger = LoggerFactory.getLogger(InMemoryAppenderExtensionTest.class);

    @BeforeEach
    void setUp() {
        logger.info("Set-up message");
    }

    @AfterEach
    void tearDown() {
        logger.info("Tear-down message");
    }

    @Test
    void testInMemAppender(InMemoryAppender<ILoggingEvent> appender) {
        logger.info("Test message");

        assertEquals(2, appender.getNumberOfEvents());
        assertTrue(appender.contains(Level.INFO, "Set-up message"));
        assertTrue(appender.contains(Level.INFO, "Test message"));
        assertFalse(appender.contains(Level.INFO, "Tear-down message"));
    }
}
